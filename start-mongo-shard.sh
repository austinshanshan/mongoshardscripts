#!/bin/zsh

if [ -z $1 ]; then
  mongos_port=27017
else
  mongos_port=$1
fi
if [ -z $2 ]; then
  num_shards=2
else
  num_shards=$2
fi

M=~/mongo/shard

rm -rf $M/data $M/logs $M/pids

mkdir -p $M/data/db/config
mkdir -p $M/logs

wait_for_mongo() {
  local port="$1"
  printf "  waiting for mongo process on port $port...";
  result=1
  while [ $result -ne 0 ]
  do
    printf ".";
    sleep 1
    mongo --port $port --eval "1" > /dev/null 2>&1
    result=$?
  done
  echo "done!";
}

mongod --config /opt/homebrew/etc/mongod.conf --fork

mongod --shardsvr --dbpath ~/mongo/shard/data/db/1 --port 1001 > $M/logs/shard1.log &
